# Base image (imagen eficiente y liviana)
FROM azul/zulu-openjdk-alpine:11.0.12-jre
# Defines work dir
WORKDIR /app
# Copy packaged artifact to work dir
ADD build/libs/usercreation-0.0.1-SNAPSHOT.jar   /app/app.jar
# Expose port 8080, used in container
EXPOSE 8080
# Command to launch container
CMD java -jar app.jar
