package cl.banco.usercreation.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.banco.usercreation.model.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Long> {

}
