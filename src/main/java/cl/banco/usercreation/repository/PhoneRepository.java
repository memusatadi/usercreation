package cl.banco.usercreation.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.banco.usercreation.model.PhoneModel;

public interface PhoneRepository extends JpaRepository<PhoneModel, Long> {

}
