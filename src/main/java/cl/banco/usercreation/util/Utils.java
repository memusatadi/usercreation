package cl.banco.usercreation.util;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.UUID;

public class Utils {

	public static BigInteger convertUUIDtoBigInteger(UUID uuid) {
		ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
		bb.putLong(uuid.getMostSignificantBits());
		bb.putLong(uuid.getLeastSignificantBits());
		return new BigInteger(1, bb.array());
	}
}
