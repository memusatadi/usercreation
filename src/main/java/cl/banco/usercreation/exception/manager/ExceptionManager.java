package cl.banco.usercreation.exception.manager;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import cl.banco.usercreation.dto.ErrorDto;
import cl.banco.usercreation.exception.EmailExistsException;

@ControllerAdvice
public class ExceptionManager {

	@ExceptionHandler
	ResponseEntity<ErrorDto> handleEmailExistsException(EmailExistsException ex) {
		ErrorDto error = ErrorDto.builder()
				.message(ex.getMessage())
				.build();
		return new ResponseEntity<>(error, HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler({MethodArgumentNotValidException.class, HttpMessageNotReadableException.class})
	ResponseEntity<ErrorDto> handleArgNotValidException(Exception ex) {
		ErrorDto error = ErrorDto.builder()
				.message(ex.getMessage())
				.build();
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler
	ResponseEntity<ErrorDto> handleServerErrorException(Exception ex) {
		ErrorDto error = ErrorDto.builder()
				.message(ex.getMessage())
				.build();
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
