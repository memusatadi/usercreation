package cl.banco.usercreation.exception;

@SuppressWarnings("serial")
public class EmailExistsException extends RuntimeException{
	
    public EmailExistsException(String message) {
        super(message);
    }

	public EmailExistsException(String message, Throwable e) {
		super(message, e);
	}

}
