package cl.banco.usercreation.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cl.banco.usercreation.dto.UserCreatedDto;
import cl.banco.usercreation.dto.UserDto;
import cl.banco.usercreation.service.UserService;

@RestController
public class UserController {
	
	@Autowired 
	private UserService userService;
	
	@PostMapping(value="/users", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserCreatedDto postUser(@Valid @RequestBody UserDto user) {
		return userService.createUser(user);
		
	}
	
}
