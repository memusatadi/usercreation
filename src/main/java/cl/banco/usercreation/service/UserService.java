package cl.banco.usercreation.service;

import cl.banco.usercreation.dto.UserCreatedDto;
import cl.banco.usercreation.dto.UserDto;

public interface UserService  {
	
	UserCreatedDto createUser(UserDto user);

}
