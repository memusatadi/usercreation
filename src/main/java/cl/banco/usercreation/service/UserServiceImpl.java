package cl.banco.usercreation.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import cl.banco.usercreation.dto.PhoneDto;
import cl.banco.usercreation.dto.UserCreatedDto;
import cl.banco.usercreation.dto.UserDto;
import cl.banco.usercreation.exception.EmailExistsException;
import cl.banco.usercreation.model.PhoneModel;
import cl.banco.usercreation.model.UserModel;
import cl.banco.usercreation.repository.PhoneRepository;
import cl.banco.usercreation.repository.UserRepository;
import cl.banco.usercreation.util.Utils;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PhoneRepository phoneRepository;

	@Override
	public UserCreatedDto createUser(UserDto user) {
		
		UUID id = UUID.randomUUID();
		
		UserModel usrModel = UserModel.builder()
				.id(Utils.convertUUIDtoBigInteger(id))
				.name(user.getName())
				.email(user.getEmail())
				.password(user.getPassword())
				.created(LocalDate.now())
				.modified(LocalDate.now())
				.lastLogin(LocalDate.now())
				.token(createToken())
				.isActive(true)
				.build();
		
		List<PhoneModel> phoneModelList = new ArrayList<>();
		
		for (PhoneDto phoneDto : user.getPhones()) {
			PhoneModel phoneModel = PhoneModel.builder()
					.phoneNumber(phoneDto.getNumber())
					.city(phoneDto.getCityCode())
					.country(phoneDto.getCountryCode())
					.user(usrModel)
					.build();
			
			phoneModelList.add(phoneModel);
		}
	
		try {
			userRepository.save(usrModel);
			phoneRepository.saveAll(phoneModelList);
		} catch (DataIntegrityViolationException e) {
			throw new EmailExistsException("El correo ya registrado", e);
		}
		
		return UserCreatedDto.builder()
				.id(id)
        		.created(usrModel.getCreated())
        		.modified(usrModel.getModified())
        		.lastLogin(usrModel.getLastLogin())
        		.token(usrModel.getToken())
        		.active(usrModel.isActive())
        		.build();
	}

	private String createToken() {
		return UUID.randomUUID().toString();
	}

}
