package cl.banco.usercreation.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "PHONE")
public class PhoneModel {
	
	@Id
	@Column(name = "PHON_NUMBER")
	private long phoneNumber;
	
	@Column(name = "PHON_CITY")
	private int city;
	
	@Column(name = "PHON_COUNTRY")
	private int country;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	private UserModel user;
}
