package cl.banco.usercreation.model;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "USER")
public class UserModel {

	@Id 
	@Column(name = "USER_ID", precision = 39)
	private BigInteger id;

	@Column(name = "USER_NAME")
	private String name;
	
	@Column(name = "USER_EMAIL", unique=true)
	private String email;

	@Column(name = "USER_PASS")
	private String password;

	@Column(name = "USER_CREATED")
	private LocalDate created;

	@Column(name = "USER_MODIFIED")
	private LocalDate modified;

	@Column(name = "USER_LAST_LOGIN")
	private LocalDate lastLogin;
	
	@Column(name = "USER_TOKEN")
	private String token;
	
	@Column(name = "USER_IS_ACTIVE")
	private boolean isActive;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<PhoneModel> phones;
	
}
