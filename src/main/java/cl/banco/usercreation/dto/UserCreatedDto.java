package cl.banco.usercreation.dto;

import java.time.LocalDate;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserCreatedDto {

	private UUID id;
	
	private LocalDate created;
	
	private LocalDate modified;
	
	@JsonProperty("last_login")
	private LocalDate lastLogin;
	
	private String token;
	
	@JsonProperty("isactive")
	private boolean active;
}
