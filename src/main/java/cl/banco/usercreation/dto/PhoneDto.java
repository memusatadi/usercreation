package cl.banco.usercreation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PhoneDto {

	private long number;
	
	@JsonProperty("citycode")
	private int cityCode;
	
	@JsonProperty("contrycode")
	private int countryCode;
}
