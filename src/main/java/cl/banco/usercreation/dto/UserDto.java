package cl.banco.usercreation.dto;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserDto {
	
	private static final String UPPER = "(?=[^A-Z]*[A-Z][^A-Z]*)";

	private static final String LOWER = "(?=.*[a-z])";

	private static final String NUM = "(?=[^0-9]*[0-9][^0-9]*[0-9][^0-9]*$)";

	private static final String LEN = ".{8,16}";


	@NonNull
	private String name;
	
	@NonNull
	@Email
	private String email;
	
	@NonNull
	@Pattern(regexp = UPPER + LOWER + NUM + LEN)
	private String password;
	
	@NonNull
	private List<PhoneDto> phones;
}
