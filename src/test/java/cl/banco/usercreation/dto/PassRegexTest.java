package cl.banco.usercreation.dto;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

public class PassRegexTest {
	
	private static final String UPPER = "(?=[^A-Z]*[A-Z][^A-Z]*)";

	private static final String LOWER = "(?=.*[a-z])";

	private static final String NUM = "(?=[^0-9]*[0-9][^0-9]*[0-9][^0-9]*$)";

	private static final String LEN = ".{8,16}";
	
	@Test
	void regex_ok() {
		
		String[] passList = new String[] {
				"pHrrr22qe", 
				"mH3r5rrtqe",
				"n4rrHrt2qe"
		};
		
		Pattern p = Pattern.compile(UPPER + LOWER + NUM + LEN);
		
		for (String pass : passList) {
			Matcher m = p.matcher(pass);
			assertTrue(m.matches(), "Password debe cumplir expresión regular: " + pass);
		}
	}
	
	@Test
	void regex_nok() {
		
		String[] passList = new String[] {
				"pHr2rr22qe", 
				"mH3roqej",
				"n4rrt2qe",
				"3r",
				"8ooooGtRyyyyyyyyyyyyyyyyyyy"
		};
		
		Pattern p = Pattern.compile(UPPER + LOWER + NUM + LEN);
		
		for (String pass : passList) {
			Matcher m = p.matcher(pass);
			assertFalse(m.matches(), "Password no debe cumplir expresión regular: " + pass);
		}
	}

}


