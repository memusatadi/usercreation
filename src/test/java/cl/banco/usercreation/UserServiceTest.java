package cl.banco.usercreation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import cl.banco.usercreation.dto.PhoneDto;
import cl.banco.usercreation.dto.UserCreatedDto;
import cl.banco.usercreation.dto.UserDto;
import cl.banco.usercreation.model.PhoneModel;
import cl.banco.usercreation.model.UserModel;
import cl.banco.usercreation.repository.PhoneRepository;
import cl.banco.usercreation.repository.UserRepository;
import cl.banco.usercreation.service.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

	@InjectMocks
    private UserServiceImpl userService;
   
    @Mock
    private UserRepository userRepository;
    
    @Mock
    private PhoneRepository phoneRepository;

    @Test
    public void userCreation_ok() {

        String name1 = "MARIA";
        String email1 = "memu@dominio.cl";
        String password1 = "passwA12";
        LocalDate created1 = LocalDate.now();
        LocalDate modified1 = LocalDate.now();
        LocalDate lastLogin1 = LocalDate.now();
        
        long phoneNumber1 = 987697775;
		int city1 = 2;
		int country1 = 56;
		
		long phoneNumber2 = 999999999;
		int city2 = 32;
		int country2 = 56;    
        
        List<PhoneModel> phonesModels = new ArrayList<>();
		
		PhoneModel phoneModel1 = PhoneModel.builder()
        		.phoneNumber(phoneNumber1)
        		.city(city1)
        		.country(country1)
        		.build();
		PhoneModel phoneModel2 = PhoneModel.builder()
        		.phoneNumber(phoneNumber2)
        		.city(city2)
        		.country(country2)
        		.build();
		
		phonesModels.add(phoneModel1);
		phonesModels.add(phoneModel2);
		
        UserModel userExpected = UserModel.builder()
        		.name(name1)
        		.email(email1)
        		.password(password1)
        		.created(created1)
        		.modified(modified1)
        		.lastLogin(lastLogin1)
        		.isActive(true)
        		.phones(phonesModels)
        		.build();
        
        List<PhoneDto> phones = new ArrayList<>();
        
        PhoneDto phone1 = PhoneDto.builder()
        		.number(phoneNumber1)
        		.cityCode(city1)
        		.countryCode(country1)
        		.build();
        
        PhoneDto phone2 = PhoneDto.builder()
        		.number(phoneNumber2)
        		.cityCode(city2)
        		.countryCode(country2)
        		.build();
		
		phones.add(phone1);
		phones.add(phone2);
		
		UserDto usr = UserDto.builder()
        		.name(name1)
        		.email(email1)
        		.password(password1)
        		.phones(phones)
        		.build();
        
        Mockito.when(userRepository.save(Mockito.any(UserModel.class))).thenReturn(userExpected);
        Mockito.when(phoneRepository.saveAll(Mockito.anyIterable())).thenReturn(phonesModels);
        
        UserCreatedDto userCreatedActual = userService.createUser(usr);
        
        assertNotNull(userCreatedActual);
        assertNotNull(userCreatedActual.getId());
        assertEquals(created1, userCreatedActual.getCreated());
        assertEquals(modified1, userCreatedActual.getModified());
        assertEquals(lastLogin1, userCreatedActual.getLastLogin());
        assertNotNull(userCreatedActual.getToken());
        assertTrue(userCreatedActual.isActive());
    }
}
